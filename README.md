# Superwave bulk uploading script for Smartlead

You can use this script to automate the integration of user sheets into Smartlead.

This script was produced by Eric Nowoslawski's team at [GrowthEngineX](https://growthenginex.com)
